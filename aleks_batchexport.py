#!/usr/bin/env python3
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from glob import glob
import numpy as np
import pandas as pd
import sys
import os


def load_file(fn):
    a = np.asarray(pd.read_table(
                   fn, delim_whitespace=True, skiprows=3, header=None,
                   names=("freq (MHz)", "Γ_real", "Γ_imag")))
    freqs = a[...,0]
    absfreqs = freqs * 1000
    nfft = 16384
    lenf = len(freqs)
    yf = np.linspace(1,(freqs[1]-freqs[0])*lenf,600)
    gammas = np.vectorize(complex)(a[...,1],a[...,2])
    imped = 50 * ((1 + gammas ) / (1 - gammas))
    capac = 1 / (2*np.pi*absfreqs*abs(imped.imag))*1e12
    return freqs, capac


def main(files, style, outname, index):
    graph = Figure(figsize=(5,2), dpi=100, facecolor="None",
            constrained_layout=True)
    spectra = graph.subplots(1,1, subplot_kw={"facecolor": (1, 1, 1, 0)})
    graph.set_constrained_layout_pads(wspace = -0.08)
    canvas = FigureCanvas(graph)
    canvas.setStyleSheet("background-color:transparent;")
    canvas.setAutoFillBackground(False)
    spectra.set_xlabel("wavelength / MHz")
    spectra.set_ylabel("capacity / pF")
    spectra.minorticks_on()
    spectra.grid(True)
    spectra.grid(True, 'minor', linewidth='0.2')
    datas = load_file(files)
    spectra.plot(datas[0], datas[1], linewidth=0.8, **style)
    print("living")
    pd.DataFrame(datas).T.to_csv(outname+".csv",header=[
                                 "wavelength / MHz", "capacity / pF"])
    print("and_now??")

    graph.savefig(outname+".jpg", dpi=300)
    graph.savefig(outname+".png", dpi=300)
    print("graphed??")

    return


if __name__ == "__main__":
    index = ""
    files = ['blue_I_justcable_800to1200kHz_std.s1p']
    files = glob("*.s1p")
    style = {'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"}
    for i in files:
        print(i)
        outname = os.path.splitext(i)[0]
        main(i, style, outname, index)
