#!/usr/bin/env python3
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import numpy as np
import pandas as pd
import sys


def load_file(fn):
    a = np.asarray(pd.read_table(
                   fn, delim_whitespace=True, skiprows=3, header=None,
                   names=("freq (MHz)", "Γ_real", "Γ_imag")))
    freqs = a[...,0]
    nfft = 16384
    lenf = len(freqs)
    yf = np.linspace(1,(freqs[1]-freqs[0])*lenf,600)
    window = np.blackman(lenf)
    gammas = np.vectorize(complex)(a[...,1],a[...,2])
    gain = 20*np.log10(np.abs(gammas))
    normgammas = gammas / np.abs(gammas)
    angles = np.angle(normgammas)
    #something = np.arcsin(a[...,1])
    something = np.power(normgammas,0.5).imag
    print(something)
    time = 1 / freqs
    
    derang = np.gradient(angles)
    goodder = np.where(derang < 0)
    selder = derang[goodder]
    avg = np.average(selder)
    #print(len(selder))
    print(np.average(selder), np.average(selder[:483]), np.average(selder[483:]))
    hist = np.histogram(derang, bins=200)
    #hist = np.histogram(derang, bins='auto')
    xavg = np.mean((hist[1][1:], hist[1][:-1]),axis=0)

    #sinang = np.tan(angles)
    absgammas = np.abs(gammas)*window
    dbgam = -np.power(10,absgammas)
    #fft = np.fft.fft(sinang)
    #print(len(fft))
    vswrs = ((1+absgammas)/(1-absgammas))


    #return freqs[goodder], angles[goodder]
    return freqs, angles


def populate(datas, plot, style):
    #plot.plot(datas[0], datas[1], color=(0,0.9,0,1))
    plot.plot(datas[0], datas[1], linewidth=0.8, **style)
    return 0


def main(pathprefix, files, styles, outname, index):
    app = QtWidgets.QApplication(sys.argv)
    graph = Figure(figsize=(5,2), dpi=100, facecolor="None",
            constrained_layout=True)
    spectra = graph.subplots(1,1, subplot_kw={"facecolor": (1, 1, 1, 0)})
    graph.set_constrained_layout_pads(wspace = -0.08)
    canvas = FigureCanvas(graph)
    canvas.setStyleSheet("background-color:transparent;")
    canvas.setAutoFillBackground(False)
    spectra.set_xlabel("wavelength(MHz)", x=1)
    spectra.set_ylabel("VSWR")
    #spectra.set_xticks(np.linspace(100,200,6))
    #spectra.set_xlim(0,40)
    #spectra.set_xlim(-0.2,0.1)
    spectra.spines['right'].set_visible(False)
    #spectra[0].set_yticks(np.linspace(1,8,8))
        #spectrum.set_ylim(0.9,100000)
    spectra.minorticks_on()
    spectra.grid(True)
    spectra.grid(True, 'minor', linewidth='0.2')
    #spectra[1].tick_params(axis='y', which='both', left=False)

    #stolen from: https://matplotlib.org/3.3.3/gallery/subplots_axes_and_figures/broken_axis.html
    d = 8  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12,
              linestyle="none", color='k', mec='k', mew=1, clip_on=False)
    spectra.plot([1, 1], [0, 1], transform=spectra.transAxes, **kwargs)

    main_window = QtWidgets.QMainWindow(windowTitle="VSWR view")
    for name, style in zip(files, styles):
        datas = load_file(pathprefix+name)
        populate(datas, spectra, style)
    #spectra.legend(loc=1)

    graph.savefig(outname+".jpg", dpi=300)
    graph.savefig(outname+".png", dpi=300)
    pixmap=QtGui.QPixmap(outname+".png")
    label = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
    main_window.setCentralWidget(label)
    main_window.resizeEvent = lambda x: label.setPixmap(
            pixmap.scaled(x.size(), QtCore.Qt.KeepAspectRatio,
                          QtCore.Qt.SmoothTransformation))
    main_window.show()
    sys.exit(app.exec_())
    return


if __name__ == "__main__":
    index = "b)"
    pathprefix = './'
    files = ['mycable_0to600mhz_fast_withoutend.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"}]
    outname = 'phaseshift'
    main(pathprefix, files, styles, outname, index)
